--------------------------------------------------------------------------------
                              API FUNCTIONS
--------------------------------------------------------------------------------
Readme for the feebase's goldenfalconAPI.
--------------------------------------------------------------------------------
/insertAccount/

    - POST
    - In: JSON object user:

            '{
                "user":{
                            "FIRSTNAME":"___", "LASTNAME":"___", "USERNAME":"___",
                            "PASSWORD":"___", "EMAIL":"___", "ISTEAMCAPTAIN":_,
                            "ISSTUDENT":_, "WAIVERSTATUS":_, "ACCESS":_}
                       }
             }'

        - ACCESS = 0 for participant, 1 for admin
        - ISSTUDENT and ISTEAMCAPTAIN not required when ACCESS=1
	-ISSTUDENT and ISTEAMCAPTIAIN are either 0 or 1
    - Out: JSON Object:

            '{"status": "___","id": "___"}'

            success:                 1
            missing parameters:      0
            no sql connection:      -1
            username exists:        -2
            query failed:           -3

--------------------------------------------------------------------------------
/login/

    - POST
    - In: JSON object creds:

            '{
                "creds":{
                            "USERNAME":"___",
                            "PASSWORD":"___"
                        }
             '}

    - Out: JSON object:

            '{ "status":_, "TOKEN":"___" }'

            - TOKEN only when status = 1
            - status:
                success:                     1
                missing parameters:          0
                no sql connection:          -1
                user not found:             -2
                wrong password:             -3
                failed to set token/access: -4

--------------------------------------------------------------------------------
/checkAccess/

    - GET
    - In: JSON object in header:

            /checkAccess/{"TOKEN":"___"}

    - Out: JSON object:

            '{"status":_, "ACCESS":_}'

            ACCESS only when status = 1
                - ACCESS = 0 for participant, 1 for admin

            success:              1
            missing parameters:   0
            no SQL connection:   -1
            token not found:     -2

--------------------------------------------------------------------------------
/checkIsCaptain/

    - GET
    - In: JSON object in header:

            /checkIsCaptain/{"TOKEN":"___"}

    - Out: JSON object:

            '{"status":_, "ISTEAMCAPTAIN":_}'

            ISTEAMCAPTAIN only when status = 1
                - ISTEAMCAPTAIN = 0 for no, 1 for yes 

            success:              1
            missing parameters:   0
            no SQL connection:   -1
            token not found:     -2

--------------------------------------------------------------------------------
/logout/

    - POST/GET?
    - In: JSON object 

            '{"TOKEN":"___"}'

    - Out: JSON object:

            '{"status":_}'

            success:              1
            missing parameters:   0
            no SQL connection:   -1
            token not found:     -2
            query failed:        -3

--------------------------------------------------------------------------------
/getUser/

    - GET
    - In: JSON object in header

            /getUser/{"TOKEN":"___","USERID":_,"USERNAME":"___"}

            - include ONLY ONE of these three parameters
            - e.x.: 

                '/getUser/{"USERID":"___"}

    - Out: JSON object:

            '{
                "status":_
                "user": [
                    {
                            "USERID":"___",
                            "FIRSTNAME":"___",
                            "LASTNAME":"___",
                            "USERNAME":"___",
                            "PASSWORD":"___",
                            "EMAIL":"___",
                            "CREATIONDATE":"___",
                            "TEAMID":_,
                            "ISTEAMCAPTAIN":_,
                            "ACCESS":_,
                            "TOKEN":"___",
                            "ISSTUDENT":_
                    }
                ]
             '}

        status = 
            success:              1
            missing parameters:   0
            no SQL connection:   -1
            query failed:        -2
             
--------------------------------------------------------------------------------
/getUsers/

    - GET
    - In: JSON object in header

        /getUser/{"EVENTID":_, "ACCESS":_, "TEAMID":_}

        These are OPTIONAL parameters. 
        Otherwise : SEND {} FOR UNFILTERED RESULTS

    - Out: JSON object: 

        - example:

            {
              "status": 1,
              "users": [
                {
                  "USERID": "1",
                  "FIRSTNAME": "JOHN",
                  "LASTNAME": "DOE",
                  "USERNAME": "STAG",
                  "PASSWORD": "BAMBIE",
                  "EMAIL": "JOHN@GMAIL.COM",
                  "CREATIONDATE": "2015-11-21 00:00:00",
                  "TEAMID": "11",
                  "ISTEAMCAPTAIN": "1",
                  "ACCESS": "0",
                  "TOKEN": null,
                  "ISSTUDENT": "0"
                },
                {
                  "USERID": "2",
                  "FIRSTNAME": "JANE",
                  "LASTNAME": "DOE",
                  "USERNAME": "DOE",
                  "PASSWORD": "BAMBIE",
                  "EMAIL": "JANE@GMAIL.COM",
                  "CREATIONDATE": "2015-11-21 00:00:00",
                  "TEAMID": "11",
                  "ISTEAMCAPTAIN": "0",
                  "ACCESS": "1",
                  "TOKEN": null,
                  "ISSTUDENT": "0"
                }
              ]
            }
    
        status = 
            success:              1
            missing parameters:   0
            no SQL connection:   -1
            query failed:        -2

--------------------------------------------------------------------------------
/insertInvitation/

    - POST
    - In: JSON object:

        '{"TOKEN":"___", "TEAMID":_, "USERID":_}'

    - Out: 403 or JSON object:

        403 if TOKEN does not correspond to user with admin or team captain 
        access 

        '{"status":_}'

        status = 
            success:              1
            missing parameters:   0
            no SQL connection:   -1
            query failed:        -2

--------------------------------------------------------------------------------
/getInvitations/

    - GET
    - In: JSON object:

        '{"TOKEN":"___"}'

    - Out: JSON object:

        - Example:

            {
              "status": 1,
              "invitations": [
                {
                  "ID": "11",
                  "TEAMID": "11",
                  "USERID": "14",
                  "TEAMCAPTAINUSERNAME": "beat"
                },
                {
                  "ID": "12",
                  "TEAMID": "12",
                  "USERID": "14"
                }
              ]
            }

    status = 
        success:                  1
        missing parameters:       0
        no sql connection:       -1
        user not found:          -2
        no invitations found     -3

--------------------------------------------------------------------------------
/rejectInvitation/

    - DELETE
    - In: JSON object:

        '{"TOKEN":"___", "INVITATIONID":_}'

    - Out: JSON object:

        '{"status":_}'

    status = 

        success:                          1
        missing parameters:               0
        no sql connection:               -1
        invitation or token not found:   -2
        delete query failed:             -3

--------------------------------------------------------------------------------
/acceptInvitation/

    - POST
    - In: JSON object:

        '{"TOKEN":"___", "INVITATIONID":_}'

    - Out: 403 or JSON object:

        Returns 403 if USERID of invitation does not match USERID of token
        *NOTE: if invited user is team captain, user becomes regular participant

        '{"status":_}'

    status = 

        success:                          1
        missing parameters:               0
        no sql connection:               -1
        invitation or token not found:   -2
        user already on a team:          -3
        add user to team query failed:   -4
        delete invitation failed:        -5
--------------------------------------------------------------------------------
/setWaiverStatus/

    - PUT
    - In: JSON object:

        /getWaiverStatus/{"USERID":_, "TOKEN":_, "USERNAME":_, "WAIVERSTATUS":_}

        Send ONLY ONE of the first three parameters

    - Out: JSON object: 

        '{"status":_, "WAIVERSTATUS":_}'

    status = 

        success:             1
        missing parameters:  0
        no sql connection:  -1
        query failed:       -2
        
--------------------------------------------------------------------------------
/getWaiverStatus/

    - GET
    - In: JSON object:

        /getWaiverStatus/{"USERID":_, "TOKEN":_, "USERNAME":_}

        Send ONLY ONE of these parameters

    - Out: JSON object: 

        '{"status":_, "WAIVERSTATUS":_}'

    status = 

        success:             1
        missing parameters:  0
        no sql connection:  -1
        query failed:       -2

--------------------------------------------------------------------------------
/insertRoute/

    - POST
    - In: JSON object:

        - Example:

            {
                "TOKEN":"1234",
                "route":
                {
                    "NAME":"patrick-test",
                    "ROUTEINFO":"muffins",
                    "ROUTETYPE": "1",
                    "EVENTID": "1",
                    "ACCESSIBLE": "0"
		    "WAYPOINTS": [
                            {
                                "waypoint_id": "3",
                                "coords": {
                                    "lat": "LAT_HERE",
                                    "long": "LONG_HERE"
                                }
                            },
                            {
                                "waypoint_id": "4",
                                "coords": {
                                    "lat": "LAT_HERE",
                                    "long": "LONG_HERE"
                                }
                            },
                            {
                                "waypoint_id": "5",
                                "coords": {
                                    "lat": "LAT_HERE",
                                    "long": "LONG_HERE"
                                }
                            }
                        ]
                }
            } 

    - Out: JSON object:

        '{"status":_,"EVENTID":_}'

    - status = 

        success:                 1
        missing parameters:      0
        no connection:          -1
        query failed:           -2

	** ACCESSIBLE 0 for not accessible 1 for accessible
	** Routetype 1 for Walking 2 for Car
---------------------------------------------------------------------------------
/getRoutes/

	-GET
	-In:NULL

	-Example:
		
	        {
  "routes": [
    {
      "ROUTEID": "2",
      "NAME": "Tstreets",
      "TEAMID": null,
      "ROUTEINFO": "muffins",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"LAT_HERE\",\"long\":\"LONG_HERE\"}}]"
    },
    {
      "ROUTEID": "3",
      "NAME": "route1",
      "TEAMID": null,
      "ROUTEINFO": "stuff",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"43.514023\",\"long\":\"-80.206051\"}}]"
    }
        ]
    }

	-OUT: JSON Object
---------------------------------------------------------------------------------
/getRoutes/:id

	-GET
	-In:User input.

	-Example:

	-Out: 
		{
  "routes": [
    {
      "ROUTEID": "2",
      "NAME": "Tstreets",
      "TEAMID": null,
      "ROUTEINFO": "muffins",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"LAT_HERE\",\"long\":\"LONG_HERE\"}}]"
    }
	]
    }

	-OUT: JSON Object
----------------------------------------------------------------------------
/getRoutes/noTeams/

        -GET
        -In:NULL

        -Example:


	{
  "routes": [
    {
      "ROUTEID": "2",
      "NAME": "Tstreets",
      "TEAMID": null,
      "ROUTEINFO": "muffins",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"LAT_HERE\",\"long\":\"LONG_HERE\"}}]",
      "ACCESSIBLE": null
    }	
	]
    }
	-OUT: JSON Object	
---------------------------------------------------------------------------------
/getRoutes/accessible/:id 

        -GET
        -In: User Input. 1 - Accessible. 0- not accessible.

        -Example:


        {
  "routes": [
    {
      "ROUTEID": "2",
      "NAME": "Tstreets",
      "TEAMID": null,
      "ROUTEINFO": "muffins",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"LAT_HERE\",\"long\":\"LONG_HERE\"}}]",
      "ACCESSIBLE": null
    }
        ]
    }
        -OUT: JSON Object 
---------------------------------------------------------------------------------
/getRoutes/event/:id

        -GET
        -In:User input

        -Example:


        {
  "routes": [
    {
      "ROUTEID": "2",
      "NAME": "Tstreets",
      "TEAMID": null,
      "ROUTEINFO": "muffins",
      "ROUTETYPE": "1",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"5\",\"coords\":{\"lat\":\"LAT_HERE\",\"long\":\"LONG_HERE\"}}]",
      "ACCESSIBLE": null
    }
        ]
    }
        -OUT: JSON Object 
---------------------------------------------------------------------------------
Purpose: Get routetype by id.
/getRoutes/routetype/:id

        -GET
        -In: User input

        -Example:
            {
  "routes": [
    {
      "ROUTEID": "17",
      "NAME": "RealRoute1",
      "TEAMID": "15",
      "ROUTEINFO": "first",
      "ROUTETYPE": "2",
      "EVENTID": "1",
      "WAYPOINTS": "[{\"waypoint_id\":\"1\",\"coords\":{\"lat\":\"43.526461\",\"long\":\"-80.224324\"}},{\"waypoint_id\":\"2\",\"coords\":{\"lat\":\"43.524423\",\"long\":\"-80.217533\"}},{\"waypoint_id\":\"3\",\"coords\":{\"lat\":\"43.520650\",\"long\":\"-80.222629\"}}]",
      "ACCESSIBLE": null
    }
  ]
}

       
        -OUT: JSON Object 
--------------------------------------------------------------------------------
/addTeamToRoute/

    - PUT
    - In: JSON object:

        '{"TOKEN":"___", "TEAMID":_, "ROUTEID":_}'

    - Out: JSON object:

        '{"status":_}'

    - status = 

        success:                 1
        missing parameters:      0
        no connection:          -1
        query failed:           -2
------------------------------------------------------------------------------------
Purpose:delete route by id.
/getRoutes/deleteroute/:id

        -GET
        -In: User input

        -OUT: success string.