<?php

$servername = "localhost";
$username = "root";
$password = "PositiveFish";
$database = "feedbase_init";

// CREATE CONNECTION
$db = new mysqli($servername, $username, $password, $database);
if ($db->connect_errno)
    echo "Connection failed: " . $db->connect_error . "<br />";
else
    echo "connected to the mysql server... <br />";

// GET TABLES
if (!($res = $db->query("SHOW TABLES")))
    echo "Query failed... " . $db->error . "<br />";
else{

    // FOR EACH TABLE
    while ($row = $res->fetch_row()){
     
        // SHOW TABLE NAME
        echo " Table: " . $row[0] . "<br />";

        // SHOW TABLE CONTENTS...

        // SHOW USERNAME ONLY
        if (!($res = $db->query("SELECT USERNAME FROM " . $row[0])))
            echo "Query failed... " . $db->error . "<br />";
        else{
            while ($row2 = $res->fetch_row()){
                foreach ($row2 as $value) 
                    echo $value . " ";
                echo "</br >";
            }
        }

        // SHOW ENTIRE ROW CONTENTS
        if (!($res = $db->query("SELECT * FROM " . $row[0])))
            echo "Query failed... " . $db->error . "<br />";
        else{
            while ($row = $res->fetch_row()){
                foreach ($row as $value) 
                    echo $value . " ";
                echo "</br >";
            }
        }
    }
}

$res->free();
$db->close();

?>
